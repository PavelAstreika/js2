// //-------exercise-1----------

// const firstname = prompt('Ваше имя'),
//       age = prompt('Ваш возраст'),
//       city = prompt('В какое городе вы проживаете?'),
//       phone = prompt('Ваш телефон'),
//       email = prompt('Ваша электронная почта'),
//       company = prompt('В какой компании работаете?');

// document.write(`<h3> Меня зовут ${firstname}. Мне ${age} лет. Я проживаю в городе ${city} и работаю в компании ${company}. Мои контактные данные: ${phone}, ${email}.</h3>`);


// // //-------exercise-2----------

// const thisYear = 2023,
//       birtYear = thisYear - age;

// document.write(`<h2> ${firstname} родился в ${birtYear} году.`)


//-------exercise-3----------

// const str = '333332';
// let sum1 = +str[0] + +str[1] + +str[2],
//     sum2 = +str[3] + +str[4] + +str[5];

// if(sum1 === sum2) {
//     console.log('да')
// } else {
//     console.log('нет')
// } 


//-------exercise-4----------

// const a = -3;

// if(a > 0) {
//     console.log('Верно')
// } else {
//     console.log('Неверно')
// }

// a > 0 ? console.log('Верно') : console.log('Неверно');


//-------exercise-5----------

// const a1 = 10,
//       b = 2;

// console.log(`${a1 + b} - сумма чисел`);
// console.log(`${a1 - b} - разность чисел`);
// console.log(`${a1 * b} - произведение чисел`);
// console.log(`${a1 / b} - частное чисел`);

// if(a1 + b > 1) console.log(`${(a1 + b) ** 2} - квадрат суммы`);


//-------exercise-6----------

// if((a1 > 2 && a1 < 11) || (b >= 6 && b < 14)) {
//     console.log('Верно') 
// } else {
//     console.log('Неверно')
// }

//-------exercise-7----------

// const n = 45;

// switch(true) {
//     case 0 <= n && n < 15:
//         document.write(`<h2>Первая четверть часа</h2>`);
//     break;
//     case 15 <= n && n < 30:
//     document.write(`<h2>Вторая четверть часа</h2>`);
//     break;
//     case 30 <= n && n < 45:
//     document.write(`<h2>Третья четверть часа</h2>`);
//     break;
//     case 45 <= n && n <= 59:
//     document.write(`<h2>Четвертая четверть часа</h2>`);
//     break;
//     default: 
//     document.write(`<h2>Введенное число вне диапазона значений</h2>`);
// }

//-------exercise-8----------


// const day =22;

// switch(true) {
//     case day >= 1 && day < 11:
//         console.log('Первая декада месяца');
//     break;
//     case day >= 11 && day < 21:
//         console.log('Вторая декада месяца');
//     break;
//     case day >= 21 && day <= 31:
//         console.log('Третья декада месяца');
//     break;
//     default:
//         console.log('Введенное число вне диапазона значений')
// }

// if(day >= 1 && day < 11) {
//     console.log('Первая декада месяца');
// } else if(day >= 11 && day < 21) {
//     console.log('Вторая декада месяца');
// } else if(day >= 21 && day <= 31) {
//     console.log('Третья декада месяца');
// } else {
//     console.log('Введенное число вне диапазона значений');
// }

//-------exercise-9----------
// version-1

// let days = 500;

// document.write(`<h2>${days} дня(дней) это:</h2>`);

// switch(true) {
//     case days >= 0 && days < 365:
//         document.write('<h2>Меньше года</h2>');
//     break;
//     case days > 365:
//         document.write(`<h2>${days / 365} лет</h2>`);
//     break;
//     default:
//         document.write(`<h2> Введено некорректное значение</h2>`);      
// }

// switch(true) {
//     case days >= 0 && days < 31:
//         document.write('<h2>Меньше месяца</h2>');
//     break;
//     case days > 31:
//         document.write(`<h2>${days / 31} месяца</h2>`);
//     break;     
// }

// switch(true) {
//     case days >= 0 && days < 7:
//         document.write('<h2>Меньше недели</h2>');
//     break;
//     case days > 7:
//         document.write(`<h2>${days / 7} недели</h2>`);
//     break;     
// }

// if(days >= 0) document.write(`<h2>${days * 24} часов</h2>`);
// if(days >= 0) document.write(`<h2>${days * 1440} минут</h2>`);
// if(days >= 0) document.write(`<h2>${days * 86400} секунд</h2>`);

// version-2

// let years = days / 365,
//     months = days / 30,
//     weeks = days / 7,
//     hours = days * 24,
//     minutes = days * 1440,
//     seconds = days * 86400;

// if( years < 1) years = 'Меньше года';
// if( months < 1) months = 'Меньше месяца';
// if( weeks < 1) weeks = 'Меньше недели';

// if(days < 0) {
//     console.log('Введено некорректное значение');
// } else {
//     console.log(
// `${days} дней это:
// в годах - ${years};
// в месяцах - ${months};
// в неделях- ${weeks};
// в часах- ${hours};
// в минутах- ${minutes};
// в секундах- ${seconds}.`);
// }

//-------exercise-10----------


// let monthNumber,
//     season;

// days = 252;

// switch(true) {
//     case days > 0 && days <= 31:
//         monthNumber = 1;
//     break;
//     case days > 31 && days <= 59:
//         monthNumber = 2;
//     break;
//     case days > 59 && days <= 90:
//         monthNumber = 3;
//     break;
//     case days > 90 && days <= 120:
//         monthNumber = 4;
//     break;
//     case days > 120 && days <= 151:
//         monthNumber = 5;
//     break;
//     case days > 151 && days <= 181:
//         monthNumber = 6;
//     break;
//     case days > 181 && days <= 212:
//         monthNumber = 7;
//     break;
//     case days > 212 && days <= 243:
//         monthNumber = 8;
//     break;
//     case days > 243 && days <= 273:
//         monthNumber = 9;
//     break;
//     case days > 273 && days <= 304:
//         monthNumber = 10;
//     break;
//     case days > 304 && days <= 334:
//         monthNumber = 11;
//     break;
//     case days > 334 && days <= 365:
//         monthNumber = 12;
//     break;
//     default:
//         monthNumber = '-';
//         season = '-';
// }

// switch(true) {
//     case monthNumber < 3 || monthNumber === 12:
//         season = 'Зима'
//     break;
//     case monthNumber >= 3  && monthNumber < 6:
//         season = 'Весна'
//     break; 
//     case monthNumber >= 6 && monthNumber < 9:
//         season = 'Лето'
//     break; 
//     case monthNumber >= 9 && monthNumber < 12:
//         season = 'Осень'
//     break;      
// }

// console.log(`№ месяца:${monthNumber}; пора года:${season}`);








